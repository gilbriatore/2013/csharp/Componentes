﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Componentes
{
    public partial class frmComponentes : Form
    {
        public frmComponentes()
        {
            InitializeComponent();
        }

        private void btnVerificarCheck_Click(object sender, EventArgs e)
        {
            if (chkOp1.Checked)
            {
                MessageBox.Show("Opção 1 marcada");
            }
            else
            {
                MessageBox.Show("Opção 1 desmarcada");
            }
            if (chkOp2.Checked)
            {
                MessageBox.Show("Opção 2 marcada");
            }
            else
            {
                MessageBox.Show("Opção 2 desmarcada");
            }
            if (chkOp3.Checked)
            {
                MessageBox.Show("Opção 3 marcada");
            }
            else
            {
                MessageBox.Show("Opção 3 desmarcada");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (rbtOp1.Checked)
            {
                MessageBox.Show("Opção 1 marcada");
            }
            else
            {
                if (rbtOp2.Checked)
                {
                    MessageBox.Show("Opção 2 marcada");
                }
                else
                {
                    MessageBox.Show("Opção 3 marcada");
                }
            }
        }

        private void btnAdicionarCombo_Click(object sender, EventArgs e)
        {
            if (!txtTexto.Text.Equals(""))
            {
                cboTexto.Items.Add(txtTexto.Text);
            }
        }

        private void btnPercorrer_Click(object sender, EventArgs e)
        {
            int i;
            for (i = 0; i < cboTexto.Items.Count; i++)
            {
                MessageBox.Show(cboTexto.Items[i].ToString());
            }
        }

        private void btnSelecionadoCombo_Click(object sender, EventArgs e)
        {
            MessageBox.Show(cboTexto.Text);
        }

        private void btnAdicionarList_Click(object sender, EventArgs e)
        {
            if (txtTextoList.Text != "")
            {
                lstTexto.Items.Add(txtTextoList.Text);
            }
        }

        private void btnPercorrerList_Click(object sender, EventArgs e)
        {
            int i;
            for (i = 0; i < lstTexto.Items.Count; i++)
            {
                MessageBox.Show(lstTexto.Items[i].ToString());
            }
        }

        private void btnSelecionadoList_Click(object sender, EventArgs e)
        {
            MessageBox.Show(lstTexto.Text);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
        }
    }
}
