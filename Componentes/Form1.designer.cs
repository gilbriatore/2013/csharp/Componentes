﻿namespace Componentes
{
    partial class frmComponentes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpCheckBox = new System.Windows.Forms.GroupBox();
            this.btnVerificarCheck = new System.Windows.Forms.Button();
            this.chkOp3 = new System.Windows.Forms.CheckBox();
            this.chkOp2 = new System.Windows.Forms.CheckBox();
            this.chkOp1 = new System.Windows.Forms.CheckBox();
            this.grpRadioButton = new System.Windows.Forms.GroupBox();
            this.btnVerificarRadio = new System.Windows.Forms.Button();
            this.rbtOp3 = new System.Windows.Forms.RadioButton();
            this.rbtOp2 = new System.Windows.Forms.RadioButton();
            this.rbtOp1 = new System.Windows.Forms.RadioButton();
            this.grpComboBox = new System.Windows.Forms.GroupBox();
            this.btnSelecionadoCombo = new System.Windows.Forms.Button();
            this.btnPercorrer = new System.Windows.Forms.Button();
            this.cboTexto = new System.Windows.Forms.ComboBox();
            this.btnAdicionarCombo = new System.Windows.Forms.Button();
            this.txtTexto = new System.Windows.Forms.TextBox();
            this.lblTexto = new System.Windows.Forms.Label();
            this.grpListBox = new System.Windows.Forms.GroupBox();
            this.lstTexto = new System.Windows.Forms.ListBox();
            this.btnSelecionadoList = new System.Windows.Forms.Button();
            this.btnPercorrerList = new System.Windows.Forms.Button();
            this.btnAdicionarList = new System.Windows.Forms.Button();
            this.txtTextoList = new System.Windows.Forms.TextBox();
            this.lblTextoList = new System.Windows.Forms.Label();
            this.grpCheckBox.SuspendLayout();
            this.grpRadioButton.SuspendLayout();
            this.grpComboBox.SuspendLayout();
            this.grpListBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpCheckBox
            // 
            this.grpCheckBox.Controls.Add(this.btnVerificarCheck);
            this.grpCheckBox.Controls.Add(this.chkOp3);
            this.grpCheckBox.Controls.Add(this.chkOp2);
            this.grpCheckBox.Controls.Add(this.chkOp1);
            this.grpCheckBox.Location = new System.Drawing.Point(12, 12);
            this.grpCheckBox.Name = "grpCheckBox";
            this.grpCheckBox.Size = new System.Drawing.Size(225, 96);
            this.grpCheckBox.TabIndex = 0;
            this.grpCheckBox.TabStop = false;
            this.grpCheckBox.Text = "CheckBox";
            // 
            // btnVerificarCheck
            // 
            this.btnVerificarCheck.Location = new System.Drawing.Point(124, 43);
            this.btnVerificarCheck.Name = "btnVerificarCheck";
            this.btnVerificarCheck.Size = new System.Drawing.Size(75, 23);
            this.btnVerificarCheck.TabIndex = 3;
            this.btnVerificarCheck.Text = "Verificar";
            this.btnVerificarCheck.UseVisualStyleBackColor = true;
            this.btnVerificarCheck.Click += new System.EventHandler(this.btnVerificarCheck_Click);
            // 
            // chkOp3
            // 
            this.chkOp3.AutoSize = true;
            this.chkOp3.Location = new System.Drawing.Point(7, 66);
            this.chkOp3.Name = "chkOp3";
            this.chkOp3.Size = new System.Drawing.Size(67, 17);
            this.chkOp3.TabIndex = 2;
            this.chkOp3.Text = "Opção 3";
            this.chkOp3.UseVisualStyleBackColor = true;
            // 
            // chkOp2
            // 
            this.chkOp2.AutoSize = true;
            this.chkOp2.Location = new System.Drawing.Point(7, 43);
            this.chkOp2.Name = "chkOp2";
            this.chkOp2.Size = new System.Drawing.Size(67, 17);
            this.chkOp2.TabIndex = 1;
            this.chkOp2.Text = "Opção 2";
            this.chkOp2.UseVisualStyleBackColor = true;
            // 
            // chkOp1
            // 
            this.chkOp1.AutoSize = true;
            this.chkOp1.Location = new System.Drawing.Point(7, 20);
            this.chkOp1.Name = "chkOp1";
            this.chkOp1.Size = new System.Drawing.Size(67, 17);
            this.chkOp1.TabIndex = 0;
            this.chkOp1.Text = "Opção 1";
            this.chkOp1.UseVisualStyleBackColor = true;
            // 
            // grpRadioButton
            // 
            this.grpRadioButton.Controls.Add(this.btnVerificarRadio);
            this.grpRadioButton.Controls.Add(this.rbtOp3);
            this.grpRadioButton.Controls.Add(this.rbtOp2);
            this.grpRadioButton.Controls.Add(this.rbtOp1);
            this.grpRadioButton.Location = new System.Drawing.Point(259, 12);
            this.grpRadioButton.Name = "grpRadioButton";
            this.grpRadioButton.Size = new System.Drawing.Size(213, 100);
            this.grpRadioButton.TabIndex = 1;
            this.grpRadioButton.TabStop = false;
            this.grpRadioButton.Text = "RadioButton";
            // 
            // btnVerificarRadio
            // 
            this.btnVerificarRadio.Location = new System.Drawing.Point(118, 43);
            this.btnVerificarRadio.Name = "btnVerificarRadio";
            this.btnVerificarRadio.Size = new System.Drawing.Size(75, 23);
            this.btnVerificarRadio.TabIndex = 4;
            this.btnVerificarRadio.Text = "Verificar";
            this.btnVerificarRadio.UseVisualStyleBackColor = true;
            this.btnVerificarRadio.Click += new System.EventHandler(this.button1_Click);
            // 
            // rbtOp3
            // 
            this.rbtOp3.AutoSize = true;
            this.rbtOp3.Location = new System.Drawing.Point(7, 65);
            this.rbtOp3.Name = "rbtOp3";
            this.rbtOp3.Size = new System.Drawing.Size(66, 17);
            this.rbtOp3.TabIndex = 2;
            this.rbtOp3.Text = "Opção 3";
            this.rbtOp3.UseVisualStyleBackColor = true;
            // 
            // rbtOp2
            // 
            this.rbtOp2.AutoSize = true;
            this.rbtOp2.Location = new System.Drawing.Point(7, 42);
            this.rbtOp2.Name = "rbtOp2";
            this.rbtOp2.Size = new System.Drawing.Size(66, 17);
            this.rbtOp2.TabIndex = 1;
            this.rbtOp2.Text = "Opção 2";
            this.rbtOp2.UseVisualStyleBackColor = true;
            // 
            // rbtOp1
            // 
            this.rbtOp1.AutoSize = true;
            this.rbtOp1.Checked = true;
            this.rbtOp1.Location = new System.Drawing.Point(7, 20);
            this.rbtOp1.Name = "rbtOp1";
            this.rbtOp1.Size = new System.Drawing.Size(66, 17);
            this.rbtOp1.TabIndex = 0;
            this.rbtOp1.TabStop = true;
            this.rbtOp1.Text = "Opção 1";
            this.rbtOp1.UseVisualStyleBackColor = true;
            // 
            // grpComboBox
            // 
            this.grpComboBox.Controls.Add(this.btnSelecionadoCombo);
            this.grpComboBox.Controls.Add(this.btnPercorrer);
            this.grpComboBox.Controls.Add(this.cboTexto);
            this.grpComboBox.Controls.Add(this.btnAdicionarCombo);
            this.grpComboBox.Controls.Add(this.txtTexto);
            this.grpComboBox.Controls.Add(this.lblTexto);
            this.grpComboBox.Location = new System.Drawing.Point(12, 118);
            this.grpComboBox.Name = "grpComboBox";
            this.grpComboBox.Size = new System.Drawing.Size(460, 123);
            this.grpComboBox.TabIndex = 2;
            this.grpComboBox.TabStop = false;
            this.grpComboBox.Text = "ComboBox";
            // 
            // btnSelecionadoCombo
            // 
            this.btnSelecionadoCombo.Location = new System.Drawing.Point(368, 83);
            this.btnSelecionadoCombo.Name = "btnSelecionadoCombo";
            this.btnSelecionadoCombo.Size = new System.Drawing.Size(75, 23);
            this.btnSelecionadoCombo.TabIndex = 8;
            this.btnSelecionadoCombo.Text = "Selecionado";
            this.btnSelecionadoCombo.UseVisualStyleBackColor = true;
            this.btnSelecionadoCombo.Click += new System.EventHandler(this.btnSelecionadoCombo_Click);
            // 
            // btnPercorrer
            // 
            this.btnPercorrer.Location = new System.Drawing.Point(368, 54);
            this.btnPercorrer.Name = "btnPercorrer";
            this.btnPercorrer.Size = new System.Drawing.Size(75, 23);
            this.btnPercorrer.TabIndex = 7;
            this.btnPercorrer.Text = "Percorrer";
            this.btnPercorrer.UseVisualStyleBackColor = true;
            this.btnPercorrer.Click += new System.EventHandler(this.btnPercorrer_Click);
            // 
            // cboTexto
            // 
            this.cboTexto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTexto.FormattingEnabled = true;
            this.cboTexto.Location = new System.Drawing.Point(322, 17);
            this.cboTexto.Name = "cboTexto";
            this.cboTexto.Size = new System.Drawing.Size(121, 21);
            this.cboTexto.TabIndex = 6;
            // 
            // btnAdicionarCombo
            // 
            this.btnAdicionarCombo.Location = new System.Drawing.Point(231, 15);
            this.btnAdicionarCombo.Name = "btnAdicionarCombo";
            this.btnAdicionarCombo.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionarCombo.TabIndex = 5;
            this.btnAdicionarCombo.Text = "Adicionar";
            this.btnAdicionarCombo.UseVisualStyleBackColor = true;
            this.btnAdicionarCombo.Click += new System.EventHandler(this.btnAdicionarCombo_Click);
            // 
            // txtTexto
            // 
            this.txtTexto.Location = new System.Drawing.Point(65, 17);
            this.txtTexto.Name = "txtTexto";
            this.txtTexto.Size = new System.Drawing.Size(160, 20);
            this.txtTexto.TabIndex = 1;
            // 
            // lblTexto
            // 
            this.lblTexto.Location = new System.Drawing.Point(7, 20);
            this.lblTexto.Name = "lblTexto";
            this.lblTexto.Size = new System.Drawing.Size(100, 23);
            this.lblTexto.TabIndex = 0;
            this.lblTexto.Text = "Texto";
            // 
            // grpListBox
            // 
            this.grpListBox.Controls.Add(this.lstTexto);
            this.grpListBox.Controls.Add(this.btnSelecionadoList);
            this.grpListBox.Controls.Add(this.btnPercorrerList);
            this.grpListBox.Controls.Add(this.btnAdicionarList);
            this.grpListBox.Controls.Add(this.txtTextoList);
            this.grpListBox.Controls.Add(this.lblTextoList);
            this.grpListBox.Location = new System.Drawing.Point(15, 247);
            this.grpListBox.Name = "grpListBox";
            this.grpListBox.Size = new System.Drawing.Size(460, 123);
            this.grpListBox.TabIndex = 3;
            this.grpListBox.TabStop = false;
            this.grpListBox.Text = "ListBox";
            // 
            // lstTexto
            // 
            this.lstTexto.FormattingEnabled = true;
            this.lstTexto.Location = new System.Drawing.Point(320, 15);
            this.lstTexto.Name = "lstTexto";
            this.lstTexto.Size = new System.Drawing.Size(120, 95);
            this.lstTexto.TabIndex = 9;
            // 
            // btnSelecionadoList
            // 
            this.btnSelecionadoList.Location = new System.Drawing.Point(231, 82);
            this.btnSelecionadoList.Name = "btnSelecionadoList";
            this.btnSelecionadoList.Size = new System.Drawing.Size(75, 23);
            this.btnSelecionadoList.TabIndex = 8;
            this.btnSelecionadoList.Text = "Selecionado";
            this.btnSelecionadoList.UseVisualStyleBackColor = true;
            this.btnSelecionadoList.Click += new System.EventHandler(this.btnSelecionadoList_Click);
            // 
            // btnPercorrerList
            // 
            this.btnPercorrerList.Location = new System.Drawing.Point(231, 53);
            this.btnPercorrerList.Name = "btnPercorrerList";
            this.btnPercorrerList.Size = new System.Drawing.Size(75, 23);
            this.btnPercorrerList.TabIndex = 7;
            this.btnPercorrerList.Text = "Percorrer";
            this.btnPercorrerList.UseVisualStyleBackColor = true;
            this.btnPercorrerList.Click += new System.EventHandler(this.btnPercorrerList_Click);
            // 
            // btnAdicionarList
            // 
            this.btnAdicionarList.Location = new System.Drawing.Point(231, 15);
            this.btnAdicionarList.Name = "btnAdicionarList";
            this.btnAdicionarList.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionarList.TabIndex = 5;
            this.btnAdicionarList.Text = "Adicionar";
            this.btnAdicionarList.UseVisualStyleBackColor = true;
            this.btnAdicionarList.Click += new System.EventHandler(this.btnAdicionarList_Click);
            // 
            // txtTextoList
            // 
            this.txtTextoList.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTextoList.Location = new System.Drawing.Point(65, 17);
            this.txtTextoList.MaxLength = 50;
            this.txtTextoList.Name = "txtTextoList";
            this.txtTextoList.Size = new System.Drawing.Size(160, 20);
            this.txtTextoList.TabIndex = 1;
            // 
            // lblTextoList
            // 
            this.lblTextoList.Location = new System.Drawing.Point(7, 20);
            this.lblTextoList.Name = "lblTextoList";
            this.lblTextoList.Size = new System.Drawing.Size(100, 23);
            this.lblTextoList.TabIndex = 0;
            this.lblTextoList.Text = "Texto";
            // 
            // frmComponentes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 394);
            this.Controls.Add(this.grpListBox);
            this.Controls.Add(this.grpComboBox);
            this.Controls.Add(this.grpRadioButton);
            this.Controls.Add(this.grpCheckBox);
            this.Name = "frmComponentes";
            this.Text = "Componentes";
            this.grpCheckBox.ResumeLayout(false);
            this.grpCheckBox.PerformLayout();
            this.grpRadioButton.ResumeLayout(false);
            this.grpRadioButton.PerformLayout();
            this.grpComboBox.ResumeLayout(false);
            this.grpComboBox.PerformLayout();
            this.grpListBox.ResumeLayout(false);
            this.grpListBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpCheckBox;
        private System.Windows.Forms.CheckBox chkOp3;
        private System.Windows.Forms.CheckBox chkOp2;
        private System.Windows.Forms.CheckBox chkOp1;
        private System.Windows.Forms.Button btnVerificarCheck;
        private System.Windows.Forms.GroupBox grpRadioButton;
        private System.Windows.Forms.Button btnVerificarRadio;
        private System.Windows.Forms.RadioButton rbtOp3;
        private System.Windows.Forms.RadioButton rbtOp2;
        private System.Windows.Forms.RadioButton rbtOp1;
        private System.Windows.Forms.GroupBox grpComboBox;
        private System.Windows.Forms.Label lblTexto;
        private System.Windows.Forms.Button btnAdicionarCombo;
        private System.Windows.Forms.TextBox txtTexto;
        private System.Windows.Forms.ComboBox cboTexto;
        private System.Windows.Forms.Button btnPercorrer;
        private System.Windows.Forms.Button btnSelecionadoCombo;
        private System.Windows.Forms.GroupBox grpListBox;
        private System.Windows.Forms.ListBox lstTexto;
        private System.Windows.Forms.Button btnSelecionadoList;
        private System.Windows.Forms.Button btnPercorrerList;
        private System.Windows.Forms.Button btnAdicionarList;
        private System.Windows.Forms.TextBox txtTextoList;
        private System.Windows.Forms.Label lblTextoList;
    }
}

